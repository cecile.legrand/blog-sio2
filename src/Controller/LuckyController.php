<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends AbstractController
{
    /**
     * @Route("/lucky/{nombre<\d+>}", name="app_lucky")
     */
    public function index(int $nombre = 1): Response
    {

        return $this->render('lucky/index.html.twig', [
            'prenom' => 'Cécile', 
            'nombre' => $nombre
        ]);
    }

    /**
     * @Route("/lucky/50", name="app_bts")
     */
    public function bts(): Response
    {
        return $this->render('lucky/bts.html.twig', [
            'ecole' => 'Lycée Dampierre',
        ]);
    }
}
