<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="article_index")
     */
    public function index(ArticleRepository $articleRepo): Response
    {
        $articles = $articleRepo->findAll();
        return $this->render('article/index.html.twig', [
            "articles" => $articles
        ]);
    }

    /**
     * @Route("/article/{id<\d+>}", name="article_view")
     */
    public function article($id, ArticleRepository $articleRepo): Response
    {
        $article = $articleRepo->find($id);
        return $this->render('article/view.html.twig', [
            'article' => $article
        ]);
    }


}
