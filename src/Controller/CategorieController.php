<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\CategorieType;
use App\Repository\CategorieRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="app_categorie")
     */
    public function index(CategorieRepository $categorieRepository): Response
    {
        $categories = $categorieRepository->findAll();
        return $this->render('categorie/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/categorie/{id<\d+>}", name="categorie_view")
     */
    public function view(CategorieRepository $categorieRepository, $id): Response
    {
        $categorie = $categorieRepository->find($id);
        return $this->render('categorie/view.html.twig', [
            'categorie' => $categorie
        ]);
    }

    /**
     * @Route("/categorieAdd", name="categorie_add")
     */
    public function add(Request $request, ManagerRegistry $doctrine): Response
    {
        $categorie = new Categorie();
        $form = $this->createForm(CategorieType::class, $categorie);

        $manager = $doctrine->getManager();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categorie = $form->getData();

            $manager->persist($categorie);
            $manager->flush(); // execute les requêtes en base

        }


            return $this->renderForm('categorie/add.html.twig', [
            'form' => $form
        ]);
    }

}
